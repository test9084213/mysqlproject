CREATE DATABASE redonbashatest;
USE redonbashatest;
CREATE TABLE dim_virus_family (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
	PRIMARY KEY (id)
);
CREATE TABLE dim_transmission (
    id int NOT NULL AUTO_INCREMENT,
    type varchar(255),
	PRIMARY KEY (id)
);
CREATE TABLE dim_region (
    id int NOT NULL AUTO_INCREMENT,
    region varchar(255),
	PRIMARY KEY (id)
);
CREATE TABLE dim_population (
    id int NOT NULL AUTO_INCREMENT,
    pop_for_1M VARCHAR(255),
	PRIMARY KEY (id)
);
CREATE TABLE dim_year (
    id int NOT NULL AUTO_INCREMENT,
    year int,
	PRIMARY KEY (id)
);
CREATE TABLE dim_month (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
	PRIMARY KEY (id)
);
CREATE TABLE dim_day (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
	PRIMARY KEY (id)
);
CREATE TABLE dim_dates (
    id int NOT NULL AUTO_INCREMENT,
    start_dt date,
    year_id int,
    month_id int,
    day_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (year_id) REFERENCES dim_year(id),
    FOREIGN KEY (month_id) REFERENCES dim_month(id),
    FOREIGN KEY (day_id) REFERENCES dim_day(id)
);
CREATE TABLE dim_country (
    id int NOT NULL AUTO_INCREMENT,
    country varchar(255),
    city_cd varchar(255),
    region_id int,
    pop_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (region_id) REFERENCES dim_region(id),
    FOREIGN KEY (pop_id) REFERENCES dim_population(id)
);
CREATE TABLE dim_virus (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    infect_rate float,
    family_id int,
    type_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (family_id) REFERENCES dim_virus_family(id),
    FOREIGN KEY (type_id) REFERENCES dim_transmission(id)
);
CREATE TABLE fact_pandemic (
    dates_id int,
    virus_id int,
    location_id int,
    cases_cnt int,
    new_cases_per1M int,
    death_cnt int,
    recovered_cnt int,
    FOREIGN KEY (dates_id) REFERENCES dim_dates(id),
	FOREIGN KEY (virus_id) REFERENCES dim_virus(id),
    FOREIGN KEY (location_id) REFERENCES dim_country(id)
);